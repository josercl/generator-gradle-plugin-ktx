package com.gitlab.josercl

import com.gitlab.josercl.generate.task.GeneratorTask
import com.gitlab.josercl.init.task.InitProjectTask
import org.gradle.api.DefaultTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class GeneratorPlugin: Plugin<Project> {
    override fun apply(project: Project) {
        val taskAction: (DefaultTask) -> Unit = { it.group = "code" }

        project.tasks.apply {
            register("generateCrud", GeneratorTask::class.java).configure(taskAction)
            register("initProject", InitProjectTask::class.java).configure(taskAction)
        }
    }
}