package com.gitlab.josercl.com.gitlab.josercl.generate.generator

import com.gitlab.josercl.com.gitlab.josercl.generate.Constants
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import kotlin.reflect.KClass
import org.apache.commons.text.CaseUtils
import org.gradle.api.Project
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

abstract class AbstractGenerator(project: Project) : IGenerator {
    protected val projectPath: String = project.projectDir.absolutePath

    protected fun createDirectories(pkg: String, modulePath: Path) {
        val path = Path.of(
            projectPath,
            modulePath.toString(),
            pkg.replace('.', File.separatorChar)
        )
        Files.createDirectories(path)
    }

    protected fun getPackage(basePackage: String?, pkg: String): String {
        basePackage ?: return pkg

        return "$basePackage.$pkg"
    }

    protected fun getEntitySpec(
        entityName: String,
        annotations: List<KClass<*>>,
        idFieldSpecs: List<PropertySpec>,
        dataClass: Boolean = false
    ): TypeSpec {
        val builder = TypeSpec.classBuilder(CaseUtils.toCamelCase(entityName, true))
            .addProperties(idFieldSpecs)
        if (dataClass) { builder.addModifiers(KModifier.DATA) }
        annotations.forEach(builder::addAnnotation)
        return builder.build()
    }

    protected fun portName(name: String): String =
        "${CaseUtils.toCamelCase(name, true)}${Constants.Domain.PORT_SUFFIX}"
}