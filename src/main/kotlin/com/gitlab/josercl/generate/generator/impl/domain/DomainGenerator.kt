package com.gitlab.josercl.com.gitlab.josercl.generate.generator.impl.domain

import com.gitlab.josercl.com.gitlab.josercl.generate.Constants
import com.gitlab.josercl.com.gitlab.josercl.generate.generator.AbstractGenerator
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import org.apache.commons.text.CaseUtils
import org.gradle.api.Project
import java.nio.file.Path

class DomainGenerator(project: Project) : AbstractGenerator(project) {
    private val domainPath = Path.of("domain/src/main/kotlin")

    override fun generate(entityName: String, basePackage: String) {
        val modelPackage = getPackage(basePackage, Constants.Domain.MODEL_PACKAGE)
        createDirectories(modelPackage, domainPath)
        val entityDomainSpec = getEntitySpec(entityName, emptyList(), emptyList(), dataClass = true)
        val domainModelFile = FileSpec.builder(modelPackage, entityDomainSpec.name!!)
            .addType(entityDomainSpec)
            .build()
        domainModelFile.writeTo(domainPath)

        val portPackage = getPackage(basePackage, Constants.Domain.SPI_PACKAGE)
        createDirectories(portPackage, domainPath)
        val portSpec = getPortSpec(entityName)
        val portFile = FileSpec.builder(portPackage, portSpec.name!!)
            .addType(portSpec).build()
        portFile.writeTo(domainPath)

        val servicePackage = getPackage(basePackage, Constants.Domain.API_PACKAGE)
        createDirectories(servicePackage, domainPath)
        val serviceSpec: TypeSpec = getServiceSpec(entityName)
        val serviceFile = FileSpec.builder(servicePackage, serviceSpec.name!!)
            .addType(serviceSpec)
            .build()
        serviceFile.writeTo(domainPath)

        val serviceImplPackage = getPackage(basePackage, Constants.Domain.API_IMPL_PACKAGE)
        createDirectories(serviceImplPackage, domainPath)
        val serviceImplSpec: TypeSpec = getServiceImplSpec(serviceFile, portFile)
        val serviceImplFile = FileSpec.builder(serviceImplPackage, serviceImplSpec.name!!)
            .addType(serviceImplSpec)
            .build()
        serviceImplFile.writeTo(domainPath)

        val exceptionPackage = getPackage(basePackage, Constants.Domain.EXCEPTION_PACKAGE)
        createDirectories(exceptionPackage, domainPath)
        val exceptionSpec: TypeSpec = getExceptionSpec(entityName)
        FileSpec.builder(exceptionPackage, exceptionSpec.name!!)
            .addType(exceptionSpec)
            .build()
            .writeTo(domainPath)
    }

    private fun getExceptionSpec(entityName: String): TypeSpec {
        val idParameterSpec: ParameterSpec = ParameterSpec.builder(
            CaseUtils.toCamelCase(String.format("%s %s", entityName, "id"), false),
            Long::class
        ).build()

        return TypeSpec.classBuilder(
            CaseUtils.toCamelCase("$entityName not found exception", true)
        )
            .superclass(ClassName(Constants.Common.EXCEPTION_PACKAGE, "RecordNotFoundException"))
            .addSuperclassConstructorParameter("%P", "$entityName not found: " + "$" + idParameterSpec.name)
            .primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameter(idParameterSpec)
                    .build()
            )
            .build()
    }

    private fun getServiceImplSpec(serviceFile: FileSpec, portFile: FileSpec): TypeSpec {
        val portName = CaseUtils.toCamelCase(portFile.name,false)
        val parameterSpec = ParameterSpec.builder(
            portName,
            ClassName(portFile.packageName, portFile.name)
        ).build()

        val propertySpec = PropertySpec.builder(portName, ClassName(portFile.packageName, portFile.name), KModifier.PRIVATE)
            .initializer(portName)
            .build()

        return TypeSpec.classBuilder(CaseUtils.toCamelCase("${serviceFile.name} Impl", true))
            .addModifiers(KModifier.PUBLIC)
            .addSuperinterface(ClassName(serviceFile.packageName, serviceFile.name))
            .primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameter(parameterSpec)
                    .build()
            )
            .addProperty(propertySpec)
            .build()
    }

    private fun getServiceSpec(entityName: String): TypeSpec {
        return TypeSpec.interfaceBuilder("${CaseUtils.toCamelCase(entityName, true)}${Constants.Domain.SERVICE_SUFFIX}")
            .addModifiers(KModifier.PUBLIC)
            .build()
    }

    private fun getPortSpec(entityName: String): TypeSpec {
        return TypeSpec.interfaceBuilder(portName(entityName))
            .addModifiers(KModifier.PUBLIC)
            .build()
    }
}