package com.gitlab.josercl.com.gitlab.josercl.generate.generator.impl.infrastructure

import com.gitlab.josercl.com.gitlab.josercl.generate.Constants
import com.gitlab.josercl.com.gitlab.josercl.generate.generator.AbstractGenerator
import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.asClassName
import com.squareup.kotlinpoet.asTypeName
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import org.apache.commons.text.CaseUtils
import org.gradle.api.Project
import org.mapstruct.Mapper
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.nio.file.Path

class InfraGenerator(project: Project) : AbstractGenerator(project) {
    private val infrastructurePath = Path.of("infrastructure/src/main/kotlin")

    override fun generate(entityName: String, basePackage: String) {
        val idFieldSpec = PropertySpec.builder("id", Long::class.asTypeName().copy(nullable = true))
            .initializer("null")
            .mutable()
            .addAnnotation(
                AnnotationSpec.builder(Id::class)
                    .useSiteTarget(AnnotationSpec.UseSiteTarget.FIELD)
                    .build()
            )
            .addAnnotation(
                AnnotationSpec.builder(GeneratedValue::class)
                    .useSiteTarget(AnnotationSpec.UseSiteTarget.FIELD)
                    .addMember("strategy = %L", "jakarta.persistence.GenerationType.IDENTITY")
                    .build()
            )
            .build()

        val entitySpec = getEntitySpec(
            "$entityName ${Constants.Infrastructure.MODEL_SUFFIX}",
            listOf(Entity::class),
            listOf(idFieldSpec)
        )

        val entityPackage = getPackage(basePackage, Constants.Infrastructure.ENTITY_PACKAGE)
        createDirectories(entityPackage, infrastructurePath)
        val entityFile = FileSpec.builder(entityPackage, entitySpec.name!!).addType(entitySpec).build()
        entityFile.writeTo(infrastructurePath)

        val repositoryPackage = getPackage(basePackage, Constants.Infrastructure.REPOSITORY_PACKAGE)
        val repositoryName = "${entityFile.name}${Constants.Infrastructure.REPOSITORY_SUFFIX}"
        createDirectories(repositoryPackage, infrastructurePath)
        val repositorySpec = getRepositorySpec(entityFile, idFieldSpec, repositoryName)
        val repositoryFile = FileSpec.builder(repositoryPackage, repositoryName)
            .addType(repositorySpec)
            .build()
        repositoryFile.writeTo(infrastructurePath)

        val mapperPackage = getPackage(basePackage, Constants.Infrastructure.MAPPER_PACKAGE)
        val mapperName = "${entityFile.name}${Constants.MAPPER_SUFFIX}"
        createDirectories(mapperPackage, infrastructurePath)
        val mapperSpec = getMapperSpec(entityFile, basePackage, mapperName)
        val mapperFile = FileSpec.builder(mapperPackage, mapperName)
            .addType(mapperSpec)
            .build()
        mapperFile.writeTo(infrastructurePath)

        val adapterPackage = getPackage(basePackage, Constants.Infrastructure.ADAPTER_PACKAGE)
        createDirectories(adapterPackage, infrastructurePath)
        val adapterSpec: TypeSpec = getAdapterSpec(entityFile, repositoryFile, mapperFile, basePackage)
        FileSpec.builder(adapterPackage, adapterSpec.name!!)
            .addType(adapterSpec)
            .build()
            .writeTo(infrastructurePath)
    }

    private fun getAdapterSpec(
        entityFile: FileSpec,
        repositoryFile: FileSpec,
        mapperFile: FileSpec,
        basePackage: String
    ): TypeSpec {
        val repositoryType = ClassName(repositoryFile.packageName, repositoryFile.name)
        val mapperType = ClassName(mapperFile.packageName, mapperFile.name)
        val portType = ClassName(
            "$basePackage.${Constants.Domain.SPI_PACKAGE}",
            portName(entityFile.name.replace(Constants.Infrastructure.MODEL_SUFFIX, ""))
        )

        val repositoryParameter = ParameterSpec.builder("repository", repositoryType).build()
        val mapperParameter = ParameterSpec.builder("mapper", mapperType).build()

        val repositoryField = PropertySpec.builder("repository", repositoryType, KModifier.PRIVATE)
            .initializer(repositoryParameter.name)
            .build()
        val mapperField = PropertySpec.builder("mapper", mapperType, KModifier.PRIVATE)
            .initializer(mapperParameter.name)
            .build()


        return TypeSpec.classBuilder("${entityFile.name.replace(Constants.Infrastructure.MODEL_SUFFIX, "")}${Constants.Infrastructure.ADAPTER_SUFFIX}")
            .addSuperinterface(portType)
            .primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameters(listOf(repositoryParameter, mapperParameter))
                .build()
            )
            .addProperties(listOf(repositoryField, mapperField))
            .build()
    }

    private fun getMapperSpec(entityFile: FileSpec, basePackage: String, mapperName: String): TypeSpec {
        val domainType = ClassName(
            "$basePackage.${Constants.Domain.MODEL_PACKAGE}",
            entityFile.name.replace(Constants.Infrastructure.MODEL_SUFFIX, "")
        )
        val entityType = ClassName(entityFile.packageName, entityFile.name)
        return TypeSpec.interfaceBuilder(mapperName)
            .addAnnotation(
                AnnotationSpec.builder(Mapper::class)
                    .addMember("injectionStrategy = %L", "org.mapstruct.InjectionStrategy.CONSTRUCTOR")
                    .addMember("componentModel = %L", "org.mapstruct.MappingConstants.ComponentModel.SPRING")
                    .build()
            )
            .addFunction(
                FunSpec.builder("toDomain")
                    .returns(domainType)
                    .addModifiers(KModifier.ABSTRACT)
                    .addParameter(
                        ParameterSpec.builder(
                            CaseUtils.toCamelCase(entityFile.name, false),
                            entityType
                        ).build()
                    )
                    .build()
            )
            .addFunction(
                FunSpec.builder("toEntity")
                    .returns(entityType)
                    .addModifiers(KModifier.ABSTRACT)
                    .addParameter(
                        ParameterSpec.builder(
                            domainType.simpleName.lowercase(),
                            domainType
                        ).build()
                    )
                    .build()
            )
            .build()
    }

    private fun getRepositorySpec(entityFile: FileSpec, idFieldSpec: PropertySpec, repositoryName: String): TypeSpec {
        val entityType = ClassName(entityFile.packageName, entityFile.name)

        return TypeSpec.interfaceBuilder(repositoryName)
            .addAnnotation(Repository::class)
            .addSuperinterface(
                CrudRepository::class.asClassName().parameterizedBy(entityType, idFieldSpec.type)
            )
            .addSuperinterface(
                JpaRepository::class.asClassName().parameterizedBy(entityType, idFieldSpec.type)
            )
            .build()
    }
}