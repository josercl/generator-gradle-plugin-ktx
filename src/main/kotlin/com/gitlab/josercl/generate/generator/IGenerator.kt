package com.gitlab.josercl.com.gitlab.josercl.generate.generator

interface IGenerator {
    fun generate(entityName: String, basePackage: String)
}