package com.gitlab.josercl.com.gitlab.josercl.generate.generator.impl.application

import com.gitlab.josercl.com.gitlab.josercl.generate.Constants
import com.gitlab.josercl.com.gitlab.josercl.generate.generator.AbstractGenerator
import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import org.apache.commons.text.CaseUtils
import org.gradle.api.Project
import org.mapstruct.Mapper
import org.springframework.web.bind.annotation.RestController
import java.nio.file.Path

class ApplicationGenerator(project: Project): AbstractGenerator(project) {
    private val applicationPath = Path.of("application/src/main/kotlin")

    override fun generate(entityName: String, basePackage: String) {
        val mapperPackage = getPackage(basePackage, Constants.Application.MAPPER_PACKAGE)
        createDirectories(mapperPackage, applicationPath)
        val mapperSpec: TypeSpec = getMapperSpec(entityName)
        val mapperFile = FileSpec.builder(mapperPackage, mapperSpec.name!!)
            .addType(mapperSpec)
            .build()
        mapperFile.writeTo(applicationPath)

        val controllerPackage = getPackage(basePackage, Constants.Application.CONTROLLER_PACKAGE)
        val controllerSpec: TypeSpec = getControllerSpec(entityName, mapperFile, basePackage)
        val controllerFile = FileSpec.builder(controllerPackage, controllerSpec.name!!)
            .addType(controllerSpec)
            .build()
        controllerFile.writeTo(applicationPath)
    }

    private fun getControllerSpec(entityName: String, mapperFile: FileSpec, basePackage: String): TypeSpec {
        val serviceType = ClassName(
            getPackage(basePackage, Constants.Domain.API_PACKAGE),
            "${CaseUtils.toCamelCase(entityName, true)}${Constants.Domain.SERVICE_SUFFIX}"
        )
        val mapperType = ClassName(mapperFile.packageName, mapperFile.name)

        val serviceParam: ParameterSpec = ParameterSpec.builder("service", serviceType).build()
        val mapperParam: ParameterSpec = ParameterSpec.builder("mapper", mapperType).build()

        val serviceProperty = PropertySpec.builder(serviceParam.name, serviceType)
            .addModifiers(KModifier.PRIVATE)
            .initializer(serviceParam.name)
            .build()
        val mapperProperty = PropertySpec.builder(mapperParam.name, mapperType)
            .addModifiers(KModifier.PRIVATE)
            .initializer(mapperParam.name)
            .build()

        return TypeSpec.classBuilder(
            CaseUtils.toCamelCase(
                "$entityName ${Constants.Application.CONTROLLER_SUFFIX}",
                true
            )
        )
            .addAnnotation(RestController::class)
            .addProperties(listOf(serviceProperty, mapperProperty))
            .primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameters(listOf(serviceParam, mapperParam))
                    .build()
            )
            .addProperties(listOf(serviceProperty, mapperProperty))
            .build()
    }

    private fun getMapperSpec(entityName: String): TypeSpec {
        return TypeSpec.interfaceBuilder(
            CaseUtils.toCamelCase(
                java.lang.String.format("%s api %s", entityName, Constants.MAPPER_SUFFIX),
                true
            )
        )
            .addAnnotation(
                AnnotationSpec.builder(Mapper::class.java)
                    .addMember("injectionStrategy = %L", "org.mapstruct.InjectionStrategy.CONSTRUCTOR")
                    .addMember("componentModel = %L", "org.mapstruct.MappingConstants.ComponentModel.SPRING")
                    .build()
            )
            .build()
    }
}