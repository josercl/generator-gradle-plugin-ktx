package com.gitlab.josercl.generate.task

import com.gitlab.josercl.com.gitlab.josercl.generate.generator.IGenerator
import com.gitlab.josercl.com.gitlab.josercl.generate.generator.impl.application.ApplicationGenerator
import com.gitlab.josercl.com.gitlab.josercl.generate.generator.impl.domain.DomainGenerator
import com.gitlab.josercl.com.gitlab.josercl.generate.generator.impl.infrastructure.InfraGenerator
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

open class GeneratorTask: DefaultTask() {
    @TaskAction
    fun run() {
        val projectProperties = project.properties

        val entities = projectProperties["entities"] ?: return

        val only = projectProperties["only"]

        val basePackage = (project.properties["basePackage"] ?: project.group) as String

        if (basePackage.equals("\$PKG", ignoreCase = true)) {
            throw RuntimeException("Init project first, execute: gradlew initProject")
        }

        val generatorsToUse = mutableListOf<IGenerator>()

        if (only == null) {
            generatorsToUse.addAll(listOf(
                DomainGenerator(project),
                InfraGenerator(project),
                ApplicationGenerator(project),
            ))
        } else {
            (only as String).split(",").map(String::trim)
                .forEach {
                    when(it) {
                        "domain" -> generatorsToUse.add(DomainGenerator(project))
                        "infra", "infrastructure" -> generatorsToUse.add(InfraGenerator(project))
                        "application", "app" -> generatorsToUse.add(ApplicationGenerator(project))
                    }
                }
        }

        (entities as String).split(",").map(String::trim)
            .forEach { entity ->
                generatorsToUse.forEach { gen ->
                    gen.generate(entity, basePackage)
                }
            }
    }
}