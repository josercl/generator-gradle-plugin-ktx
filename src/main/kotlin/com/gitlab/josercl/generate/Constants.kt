package com.gitlab.josercl.com.gitlab.josercl.generate

object Constants {
    const val MAPPER_SUFFIX = "Mapper"

    object Infrastructure {
        const val ENTITY_PACKAGE = "persistence.entity"
        const val MAPPER_PACKAGE = "persistence.entity.mapper"
        const val REPOSITORY_PACKAGE = "persistence.repository"
        const val ADAPTER_PACKAGE = "persistence.adapter"
        const val MODEL_SUFFIX = "Entity"
        const val REPOSITORY_SUFFIX = "Repository"
        const val ADAPTER_SUFFIX = "Adapter"
    }

    object Common {
        const val EXCEPTION_PACKAGE = "common.exception"
    }

    object Domain {
        const val MODEL_PACKAGE = "domain.model";
        const val SPI_PACKAGE = "domain.spi";
        const val API_PACKAGE = "domain.api";
        const val EXCEPTION_PACKAGE = "domain.exception";
        const val API_IMPL_PACKAGE = "$API_PACKAGE.impl";
        const val SERVICE_SUFFIX = "Service";
        const val PORT_SUFFIX = "Port";
    }

    object Application {
        const val CONTROLLER_SUFFIX = "Controller";
        const val CONTROLLER_PACKAGE = "application.rest.controller";
        const val MAPPER_PACKAGE = "application.rest.model.mapper";
    }
}