package com.gitlab.josercl.init.task

import com.gitlab.josercl.com.gitlab.josercl.init.creator.impl.ApplicationConfigurationCreator
import com.gitlab.josercl.com.gitlab.josercl.init.creator.impl.BasePageMapperCreator
import com.gitlab.josercl.com.gitlab.josercl.init.creator.impl.DomainPageCreator
import com.gitlab.josercl.com.gitlab.josercl.init.creator.impl.ErrorHandlerCreator
import com.gitlab.josercl.com.gitlab.josercl.init.creator.impl.PersistenceConfigurationCreator
import com.gitlab.josercl.init.creator.impl.MainApplicationCreator
import com.gitlab.josercl.init.exception.InitException
import com.squareup.kotlinpoet.FileSpec
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption

open class InitProjectTask: DefaultTask() {

    private val modulesDirs = mapOf(
        "boot" to emptyList(),
        "infrastructure" to listOf("persistence/adapter", "persistence/config", "persistence/entity/mapper", "persistence/repository"),
        "domain" to listOf("domain/api/impl", "domain/exception", "domain/model", "domain/spi"),
        "application" to listOf("application/configuration", "application/rest/controller", "application/rest/model/mapper")
    )

    private val projectPath = project.projectDir.absolutePath
    private val mainApplicationCreator: MainApplicationCreator = MainApplicationCreator(project)
    private val applicationConfigurationCreator: ApplicationConfigurationCreator = ApplicationConfigurationCreator(project)
    private val persistenceConfigurationCreator: PersistenceConfigurationCreator = PersistenceConfigurationCreator(project)
    private val errorHandlerCreator: ErrorHandlerCreator = ErrorHandlerCreator(project)
    private val domainPageCreator: DomainPageCreator = DomainPageCreator(project)
    private val basePageMapperCreator: BasePageMapperCreator = BasePageMapperCreator(project)

    @TaskAction
    fun run() {
        val basePackage: String = project.properties["basePackage"] as? String
            ?: throw InitException()

        val projectName: String = project.properties["projectName"] as? String
            ?: throw InitException()

        val baseFolder = basePackage.replace('.', File.separatorChar)

        writeSettingsGradleFile(projectName)
        writeBuildGradleFile(basePackage)
        initDirectories(baseFolder)
        createClasses(basePackage)
    }

    private fun writeSettingsGradleFile(projectName: String) {
        val settingsGradlePath = Path.of(projectPath, "settings.gradle")
        val bytes = Files.readAllBytes(settingsGradlePath.toAbsolutePath())
        val content = String(bytes).replace("spring-boot-gradle-template-ktx", projectName)
        Files.writeString(settingsGradlePath, content, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING)
    }

    private fun writeBuildGradleFile(basePackage: String) {
        val buildGradlePath = Path.of(projectPath, "build.gradle")
        val bytes = Files.readAllBytes(buildGradlePath.toAbsolutePath())
        val content = String(bytes).replace("\$PKG", basePackage)
        Files.writeString(buildGradlePath, content, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING)
    }

    private fun initDirectories(baseFolder: String) {
        modulesDirs.flatMap {
            it.value.map { dir ->
                Path.of(projectPath, it.key, "src", "main", "kotlin", baseFolder, dir)
            }
        }.forEach { path ->
            runCatching {
                Files.createDirectories(path)
            }.onFailure { it.printStackTrace() }
        }
    }

    private fun createClasses(basePackage: String) {
        mainApplicationCreator.createClass(basePackage)
        persistenceConfigurationCreator.createClass(basePackage)
        errorHandlerCreator.createClass(basePackage)
        applicationConfigurationCreator.createClass(basePackage)
        val domainPageFile: FileSpec = domainPageCreator.createClass(basePackage)
        basePageMapperCreator.createClass(basePackage, domainPageFile)
    }
}