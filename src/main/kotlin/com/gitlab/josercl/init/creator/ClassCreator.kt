package com.gitlab.josercl.init.creator

import com.squareup.kotlinpoet.FileSpec
import org.gradle.api.Project

abstract class ClassCreator(project: Project) {

    protected val projectPath = project.projectDir.absolutePath

    abstract fun createClass(basePackage: String, vararg deps: FileSpec): FileSpec

    fun createClass(basePackage: String): FileSpec {
        return createClass(basePackage, *arrayOf())
    }
}