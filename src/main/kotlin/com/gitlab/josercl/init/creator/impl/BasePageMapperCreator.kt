package com.gitlab.josercl.com.gitlab.josercl.init.creator.impl

import com.gitlab.josercl.init.creator.ClassCreator
import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.TypeVariableName
import com.squareup.kotlinpoet.asClassName
import org.gradle.api.Project
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.springframework.data.domain.Page
import java.nio.file.Path

class BasePageMapperCreator(project: Project) : ClassCreator(project) {
    override fun createClass(basePackage: String, vararg deps: FileSpec): FileSpec {
        val domainPageFile = deps[0]
        val t = TypeVariableName("T")
        val page = ParameterSpec.builder("page", Page::class.asClassName().parameterizedBy(t)).build()
        val domainPage = ClassName(domainPageFile.packageName, domainPageFile.name)
        val name = "BasePageMapper"

        val mappings = listOf(
            AnnotationSpec.builder(Mapping::class.asClassName())
                .addMember("target = %S", "pageSize")
                .addMember("source = %S", "size")
                .build(),
            AnnotationSpec.builder(Mapping::class.asClassName())
                .addMember("target = %S", "page")
                .addMember("source = %S", "number")
                .build(),
            AnnotationSpec.builder(Mapping::class.asClassName())
                .addMember("target = %S", "content")
                .addMember("source = %S", "content")
                .addMember("defaultExpression = %S", "java(emptyList())")
                .build()
        )

        val pageMapperSpec = TypeSpec.Companion.interfaceBuilder(name)
            .addTypeVariable(t)
            .addFunction(
                FunSpec.builder("toDomainPage")
                    .addModifiers(KModifier.ABSTRACT)
                    .addParameter(page)
                    .returns(domainPage.parameterizedBy(t))
                    .addAnnotation(
                        AnnotationSpec.builder(Mappings::class)
                            .addMember("%T(target = \"pageSize\", source = \"size\")", Mapping::class)
                            .addMember("%T(target = \"page\", source = \"number\")", Mapping::class)
                            .addMember("%T(target = \"content\", source = \"content\", defaultExpression=\"java(java.util.List.of())\")", Mapping::class)
                            .build()
                    )
                    .build()
            )
            .build()

        val destPackage = "$basePackage.persistence.entity.mapper"
        val fileSpec = FileSpec.builder(destPackage, name)
            .addType(pageMapperSpec)
            .build()

        fileSpec.writeTo(
            Path.of(
                projectPath,
                "infrastructure",
                "src", "main", "kotlin"
            )
        )

        return fileSpec
    }
}
