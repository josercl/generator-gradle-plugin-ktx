package com.gitlab.josercl.com.gitlab.josercl.init.creator.impl

import com.gitlab.josercl.init.creator.ClassCreator
import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.asClassName
import com.squareup.kotlinpoet.asTypeName
import com.squareup.kotlinpoet.jvm.throws
import org.gradle.api.Project
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.lang.reflect.Modifier
import java.nio.file.Path

class ErrorHandlerCreator(project: Project) : ClassCreator(project) {
    override fun createClass(basePackage: String, vararg deps: FileSpec): FileSpec {
        val name = "ErrorHandler"
        val errorHandlerSpec = TypeSpec.classBuilder(name)
            .superclass(ResponseEntityExceptionHandler::class)
            .addAnnotation(ControllerAdvice::class)
            .addFunction(getHandleResponseExceptionMethod(basePackage))
            .addFunction(getHandleMethodArgumentNotValid())
            .build()

        val destPackage = "$basePackage.application"
        val fileSpec = FileSpec.builder(destPackage, name)
            .addType(errorHandlerSpec)
            .build()

        fileSpec.writeTo(Path.of(
            projectPath,
            "application",
            "src", "main", "kotlin"
        ))

        return fileSpec
    }

    private fun getHandleMethodArgumentNotValid(): FunSpec {
        val baseMethod = ResponseEntityExceptionHandler::class.java.declaredMethods.first {
            it.name.equals("handleMethodArgumentNotValid", ignoreCase = true)
        }

        val builder = FunSpec.builder(baseMethod.name)
            .addModifiers(KModifier.OVERRIDE)

        val modifiers = baseMethod.modifiers
        val modifierSet = mutableSetOf<KModifier>()
        when {
            Modifier.isPublic(modifiers) -> modifierSet.add(KModifier.PUBLIC)
            Modifier.isProtected(modifiers) -> modifierSet.add(KModifier.PROTECTED)
            Modifier.isPrivate(modifiers) -> modifierSet.add(KModifier.PRIVATE)
            Modifier.isAbstract(modifiers) -> modifierSet.add(KModifier.ABSTRACT)
            Modifier.isFinal(modifiers) -> modifierSet.add(KModifier.FINAL)
            Modifier.isVolatile(modifiers) -> builder.addAnnotation(Volatile::class)
            Modifier.isSynchronized(modifiers) -> builder.addAnnotation(Synchronized::class)
        }

        builder.addModifiers(modifierSet)
            .returns(baseMethod.returnType.asClassName().parameterizedBy(Any::class.asTypeName()))

        var exParameter: ParameterSpec? = null
        baseMethod.parameters.forEach {
            val parameterSpec = ParameterSpec.builder(
                it.name, it.type
            ).build()

            if (MethodArgumentNotValidException::class.java.isAssignableFrom(it.type)) {
                exParameter = parameterSpec
            }

            builder.addParameter(parameterSpec)
        }

        exParameter ?: throw RuntimeException("MethodArgumentNotValidException argument not found")

        baseMethod.exceptionTypes.forEach {
            builder.throws(it)
        }

        builder.addStatement("""
            val errorList = %N.bindingResult
            .fieldErrors
            .groupBy({ it.field }, { it.defaultMessage.orEmpty() })
            .map { %T(it.key, it.value) }
        return ResponseEntity.unprocessableEntity().body(errorList)
        """.trimIndent(), exParameter!!, ClassName("common", "ValidationError"))

        return builder.build()
    }

    private fun getHandleResponseExceptionMethod(basePackage: String): FunSpec {
        val responseException = ClassName("common.exception", "ResponseException")
        val errorDTO = ClassName("$basePackage.rest.server.model", "ErrorDTO")
        val exArgument = ParameterSpec.builder("ex", responseException).build()
        val responseEntity = ClassName("org.springframework.http", "ResponseEntity")

        return FunSpec.builder("handleResponseExcepton")
            .addAnnotation(
                AnnotationSpec.builder(ExceptionHandler::class)
                    .addMember("%T::class", responseException)
                    .build()
            )
            .addModifiers(KModifier.PROTECTED)
            .returns(responseEntity.parameterizedBy(errorDTO))
            .addParameter(exArgument)
            .addStatement("""
                return %T.status(%N.code)
                .body(
                    %T().code(%N.code.value())
                    .message(%N.message)
                )
            """.trimIndent(), responseEntity, exArgument, errorDTO, exArgument, exArgument)
            .build()
    }
}