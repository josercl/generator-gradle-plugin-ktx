package com.gitlab.josercl.com.gitlab.josercl.init.creator.impl

import com.gitlab.josercl.init.creator.ClassCreator
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.TypeSpec
import org.gradle.api.Project
import org.springframework.context.annotation.Configuration
import java.nio.file.Path

class ApplicationConfigurationCreator(project: Project) : ClassCreator(project) {
    override fun createClass(basePackage: String, vararg deps: FileSpec): FileSpec {
        val name = "ApplicationConfiguration"

        val applicationConfigurationSpec = TypeSpec.classBuilder(name)
            .addAnnotation(Configuration::class)
            .build()

        val destPackage = "$basePackage.application.configuration"

        val fileSpec = FileSpec.builder(destPackage, name)
            .addType(applicationConfigurationSpec)
            .build()

        fileSpec.writeTo(Path.of(
            projectPath,
            "application",
            "src", "main", "kotlin"
        ))

        return fileSpec
    }
}
