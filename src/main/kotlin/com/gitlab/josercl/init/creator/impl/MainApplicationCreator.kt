package com.gitlab.josercl.init.creator.impl

import com.gitlab.josercl.init.creator.ClassCreator
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeSpec
import org.gradle.api.Project
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.nio.file.Path

class MainApplicationCreator(project: Project) : ClassCreator(project) {

    override fun createClass(basePackage: String, vararg deps: FileSpec): FileSpec {
        val mainApplicationName = "MainApplication"

        val mainApplicationSpec = TypeSpec.classBuilder(mainApplicationName)
            .addAnnotation(SpringBootApplication::class)
            .build()

        val mainParamSpec = ParameterSpec.builder("args", Array::class.parameterizedBy(String::class))
            .build()

        val mainFunctionSpec = FunSpec.builder("main")
            .addParameter(mainParamSpec)
            .addStatement("%T<%L>(*%N)", ClassName("org.springframework.boot", "runApplication"), mainApplicationName, mainParamSpec)
            .build()

        val mainApplicationFileSpec = FileSpec.builder(basePackage, mainApplicationName)
            .addType(mainApplicationSpec)
            .addFunction(mainFunctionSpec)
            .build()

        mainApplicationFileSpec.writeTo(
            Path.of(
                projectPath,
                "boot",
                "src", "main", "kotlin"
            )
        )

        return mainApplicationFileSpec
    }
}