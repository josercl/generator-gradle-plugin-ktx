package com.gitlab.josercl.com.gitlab.josercl.init.creator.impl

import com.gitlab.josercl.init.creator.ClassCreator
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.TypeSpec
import org.gradle.api.Project
import org.springframework.context.annotation.Configuration
import java.nio.file.Path

class PersistenceConfigurationCreator(project: Project) : ClassCreator(project) {

    override fun createClass(basePackage: String, vararg deps: FileSpec): FileSpec {
        val name = "PersistenceConfiguration"
        val persistenceConfigurationSpec = TypeSpec.classBuilder(name)
            .addAnnotation(Configuration::class)
            .build()

        val destPackage = "$basePackage.persistence.config"

        val fileSpec = FileSpec.builder(destPackage, name)
            .addType(persistenceConfigurationSpec)
            .build()

        fileSpec.writeTo(
            Path.of(
                projectPath,
                "infrastructure",
                "src", "main", "kotlin"
            )
        )

        return fileSpec
    }
}