package com.gitlab.josercl.com.gitlab.josercl.init.creator.impl

import com.gitlab.josercl.init.creator.ClassCreator
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.TypeVariableName
import com.squareup.kotlinpoet.asClassName
import org.gradle.api.Project
import java.nio.file.Path

class DomainPageCreator(project: Project) : ClassCreator(project) {
    override fun createClass(basePackage: String, vararg deps: FileSpec): FileSpec {
        val name = "DomainPage"
        val typeVariable = TypeVariableName("T")
        val domainPageSpec = TypeSpec.classBuilder(name)
            .addTypeVariable(typeVariable)
            .addModifiers(KModifier.DATA)
            .primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameter("content", List::class.asClassName().parameterizedBy(typeVariable))
                    .addParameter("page", Int::class)
                    .addParameter("pageSize", Int::class)
                    .addParameter("totalElements", Long::class)
                    .addParameter("totalPages", Int::class)
                    .build()
            )
            .addProperty(
                PropertySpec.builder("content", List::class.asClassName().parameterizedBy(typeVariable)).initializer("content").build()
            )
            .addProperty(PropertySpec.builder("page", Int::class).initializer("page").build())
            .addProperty(PropertySpec.builder("pageSize", Int::class).initializer("pageSize").build())
            .addProperty(PropertySpec.builder("totalElements", Long::class).initializer("totalElements").build())
            .addProperty(PropertySpec.builder("totalPages", Int::class).initializer("totalPages").build())
            .build()

        val destPackage = "$basePackage.domain.model"

        val fileSpec = FileSpec.builder(destPackage, name)
            .addType(domainPageSpec)
            .build()

        fileSpec.writeTo(Path.of(
            projectPath,
            "domain",
            "src", "main", "kotlin"
        ))

        return fileSpec
    }
}
