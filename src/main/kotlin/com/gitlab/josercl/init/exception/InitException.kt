package com.gitlab.josercl.init.exception

class InitException : RuntimeException("Usage: gradle initProject -PbasePackage=xxx.yyy.zzz -PprojectName=this_is_the_project_name")
